## NetSuite SDF SuiteApp Scaffolding

Project scaffold for SDF SuiteApp projects

#### SDF SuiteApp: Example Directory structure
```
/
  src/ - Contains all source code for the SuiteApp
    FileCabinet/SuiteApps/ - Corresponds to the SuiteApps directory of the NetSuite account
      publisherid.appid/ - The top-level directory for your SuiteApp source. Change the name of this directory to match your actual Publisher ID and App ID
        lib/ - Common/helper modules for your SuiteApp
        module1/ - source files for module named "module1"
          mySuitelet.js - Suitelet entry point script
        anotherModule/ - source files for module named "anotherModule"
          aUserEvent.js - User Event entry point script
    Objects/ - Contains all SDF Object definitions for the SuiteApp
    manifest.xml - SuiteApp manifest file
    deploy.xml - SuiteApp deployment file
  test/ - Test source
    module1/ - test files for module named "module1"
        doCalculationTest.js - test file for "doCalculation" function in module1
        doAnotherCalculationTest.js - test file for "doAnotherCalculation" function in module1
    anotherModule/ - test files for "anotherModule" module
  doc/ - Documentation for your SuiteApp
```

*Setup*

1. Modify `manifest.xml` with your Publisher and SuiteApp IDs, your Project Name, and the Version
1. Rename the `publisherid.appid` directory to match your actual Publisher and SuiteApp IDs
1. Modify `deploy.xml` with the correct `files` path
1. Run `npm install`

*Recommendations*

* Place your NetSuite entry-point source files (e.g. User Events, Suitelets, etc) in the root of the SuiteApp source folder
* Extract the common business logic into library files/modules, and place them in their own module folders under `lib`
* Organize your `test` directory by the file and function under test

#### Default dependencies
* [eslint](https://www.npmjs.com/package/eslint) - Code linter
* [eslint-plugin-suitescript](https://www.npmjs.com/package/eslint-plugin-suitescript) - Environment plugin for `eslint` that adds SuiteScript globals
* [jsdoc](https://www.npmjs.com/package/jsdoc) - Documentation generator
* [jsdoc-plugin-suitescript](https://www.npmjs.com/package/jsdoc-plugin-suitescript) - NetSuite tag support for jsdoc
* [mocha](https://www.npmjs.com/package/mocha) - Test framework
* [should](https://www.npmjs.com/package/should) - BDD-style assertion library
